import { NgModule } from '@angular/core';

import { ElasticdemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [ElasticdemoSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [ElasticdemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ElasticdemoSharedCommonModule {}
